export default (input, output = null) => ({
  ...baseConfig,
  input,
  output: {
    ...(output
      ? {
          file: path.join('dist', `${output}.js`),
        }
      : { entryFileNames: '[name].js', dir: 'dist' }),
    format: 'esm',
    exports: 'named',
    sourcemap: true,
    chunkFileNames: '_chunks/[name].[hash].js',
  },
  plugins: [
    ...baseConfig.plugins.preVue,
    vue(baseConfig.plugins.vue),
    ...baseConfig.plugins.postVue,
    babel({
      ...baseConfig.plugins.babel,
      presets: [
        [
          '@babel/preset-env',
          {
            targets: esbrowserslist,
            modules: false,
          },
        ],
      ],
    }),
    commonjs(),
  ],
});