import compositionApi from '@vue/composition-api';
import * as components from '../src/components';

/**
 * Install all components
 *
 * @param  {Vue}  Vue The Vue object
 * @return {void}
 */
export let _instance:any;

export default (instance: any) => {
  instance.use(compositionApi);
  Object.keys(components).forEach((name) => {
    instance.component(name, (components as any)[name]);
  });
};